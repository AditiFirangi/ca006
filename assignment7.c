#include<stdio.h>
char input_grade(int i)
{
char grade;
printf("Enter %d student's grade\n", i);
scanf(" %c", &grade);
return grade;
}
int compute(char grade)
{
int z=0;
if(grade=='S' || grade=='A' || grade=='B')
z=1;
else if (grade=='E' || grade=='F')
z=2;
return z;
}
int main()
{
int i, n, z=0, x=0,y=0;
char grade;
printf("Enter the number of students\n");
scanf("%d", &n);
for(i=0; i<n; i++)
{
grade=input_grade(i);
z=compute(grade);
if(z==1)
x++;
else if(z==2)
y++;
}
printf("\n The number of students scoring above 70 is %d", x);
printf("\nThe number of students scoring below 50 is %d", y);
return 0;
}
