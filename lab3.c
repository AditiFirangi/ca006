#include<stdio.h.
#include<math.h.
int main()
{
int a,b,c;
float x1,x2,d;
printf("Enter the values of a, b and c for the quadratic equation a*x*x+b*x+c=0\n");
scanf("%d%d%d", &a, &b, &c);
d=b*b-4*a*c;
if(d==0)
{
x1=b/(2*a);
x2=b/(2*a);
printf("The roots are equal and real %f and %f\n", x1, x2);
}
else if(d>0)
{
x1=(b+sqrt(b*b-4*a*c))/(2*a);
x2=(b-sqrt(b*b-4*a*c))/(2*a);
printf("The roots are distinct and real %f and %f\n", x1, x2);
}
else 
{
printf("The roots are imaginary %f and %f\n",x1, x2);
}
return 0;
}
